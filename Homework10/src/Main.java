/**
 * 14.03.2021
 * Homework10
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Main {

    public static void main(String[] args) {
        //Инициализация объектов
        InnoList arrayList = new InnoArrayList();
        InnoList linkedList = new InnoLinkedList();
        // Заполнение значениями
        for (int i = 1; i < 10; i++) {
            arrayList.add(i);
            linkedList.add(i);
        }

        //Вывод изначального массива
        System.out.println(arrayList);
        //Метод добавления в начало
        arrayList.addToBegin(99);
        //Метод удаления по индексу
        arrayList.removeByIndex(5);
        //Метод удаления конкретного значения
        arrayList.remove(7);
        //Метод определения наличия значения в массиве
        System.out.println(arrayList.contains(8));
        //Метод добавления элемента в конкретный индекс
        arrayList.insert(3, -23);
        //Вывод измененного массива
        System.out.println(arrayList);

        //Вывод изначального представления узлов
        ((InnoLinkedList) linkedList).print();
        // Метод определения наличия значения в узлах
        System.out.println(linkedList.contains(9));
        //Метод удаления конкретного значения
        linkedList.remove(6);
        //Метод добавления в начало
        linkedList.addToBegin(3);
        //Метод удаления конкретного узла
        linkedList.removeByIndex(3);
        //Метод добавления элемента в узел по индексу
        linkedList.insert(5, 99);
        //Вывод измененного представления узлов
        ((InnoLinkedList) linkedList).print();
    }
}

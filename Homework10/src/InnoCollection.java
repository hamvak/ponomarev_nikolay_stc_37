/**
 * 14.03.2021
 * Homework10
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
//Коллекция - контейнер элементов
public interface InnoCollection {
    /**
     * Добавляет элемент в коллекцию
     * @param element добавляемый элемент
     */
    void add(int element);

    /**
     * Удаляет элемент из коллекции
     * @param element удаляемый элемент
     */

    void remove(int element);

    /**
     * Проверяет наличие элемента в коллекции
     * @param element искомый элемент
     * @return true , если элемент найден, false в противном случае
     */

    boolean contains (int element);

    /**
     * Количество элементов коллекции
     * @return значение, равное количеству элементов
     */
    int size();
}

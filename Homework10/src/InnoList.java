
/**
 * 14.03.2021
 * Homework10
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
// Список - это коллекция, которая подразумевает порядок элементов
public interface InnoList extends InnoCollection {
    /**
     * Получение элемента по индексу
     *
     * @param index индекс элемента
     * @return элемент, -1 если вышли за границу
     */
    int get(int index);

    /**
     * Замена элемента в определенном индексе
     *
     * @param index   куда хотим вставить элемент
     * @param element вставляемый элемент
     */

    void insert(int index, int element);

    /**
     * Добавление элемента в начало
     *
     * @param element
     */
    void addToBegin(int element);

    /**
     * Удаляет элемент в заданной позиции
     *
     * @param index
     */
    void removeByIndex(int index);

}


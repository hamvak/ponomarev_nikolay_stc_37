import java.util.Arrays;
/**
 * 14.03.2021
 * Homework10
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;
    private int elements[];

    private int count;


    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    /**
     * Метод вывода массива
     *
     * @return
     */
    @Override
    public String toString() {
        return "InnoArrayList{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }

    /**
     * Метод удаляет конкретное значение в массиве
     *
     * @param element удаляемый элемент
     */
    @Override
    public void remove(int element) {
        //Создается пустой временный массив с размером меньшим, чем изначальный массив
        int[] temp = new int[elements.length - 1];
        int i;
        //Если элемент найден в массиве, то заполняем временный массив значениями изначального массива
        //До того индекса, в котором был найденный элемент
        if (contains(element)) {
            for (i = 0; i < elements.length; i++) {
                temp[i] = elements[i];
                if (elements[i] == element) {
                    break;
                }
            }
            // Затем дозаполняем временный массив  остальными элементами изначального массива, в обход найденного элемента
            // И приравниваем изначальный массив к временному
            for (int k = i; k < elements.length - 1; k++) {
                temp[k] = elements[k + 1];
            }
            elements = temp;
        } else {
            System.err.println("Element did not found");
        }
    }

    /**
     * Метод определения наличия элемента в массиве
     *
     * @param element искомый элемент
     * @return
     */
    @Override
    public boolean contains(int element) {
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    /**
     * Метод добавления элемента в начало
     *
     * @param element
     */
    @Override
    public void addToBegin(int element) {
        //Создается пустой временный массив с размером большим, чем изначальный массив
        int[] temp = new int[elements.length + 1];
        //Вставляем в нулевую ячейку нужное значение и дозаполняем элементами изначального массива
        //И приравниваем изначальный массив к временному
        for (int i = 1; i <= elements.length; i++) {
            temp[0] = element;
            temp[i] = elements[i - 1];
        }
        elements = temp;
    }

    /**
     * Метод удаления по индексу
     *
     * @param index
     */
    @Override
    public void removeByIndex(int index) {
        //Создается пустой временный массив с размером меньшим, чем изначальный массив
        int[] temp = new int[elements.length - 1];
        if (index < 0 || index > elements.length - 1) {
            System.err.println("Incorrect index");
        } else {
            int i;
            //Заполняем временный массив элементами изначального массива
            //До того момента, пока не дойдем до нужной ячейки
            for (i = 0; i < index; i++) {
                temp[i] = elements[i];
            }
            //Дозаполняем временный массив, в обход удаляемой ячейки
            //И приравниваем изначальный массив к временному
            for (int k = i; k < elements.length - 1; k++) {
                temp[k] = elements[k + 1];
            }
            elements = temp;
        }
    }

    /**
     * Метод добавления элемента в конкретный индекс
     *
     * @param index   куда хотим вставить элемент
     * @param element вставляемый элемент
     */
    @Override
    public void insert(int index, int element) {
        //Создается пустой временный массив с размером большим, чем изначальный массив
        int[] temp = new int[elements.length + 1];
        if (index < 0 || index > elements.length - 1) {
            System.err.println("Incorrect index");
        }
        //Заполняем временный массив элементами изначального массива до нужной нам ячейки
        //Присваиваем значение нужной нам ячейки и дозаполняем временный массив
        //И приравниваем изначальный массив к временному
        else {
            int i;
            for (i = 0; i < index; i++) {
                temp[i] = elements[i];
                temp[index] = element;
            }
            for (int k = i; k < elements.length; k++) {
                temp[k + 1] = elements[k];
            }
            elements = temp;
        }

    }

    @Override
    public void add(int element) {
        //если список переполнен
        if (count == elements.length) {
            resize();
        }
        elements[count++] = element;
    }

    private void resize() {
        //создаем масиив в полтора раза больший
        int[] newElements = new int[elements.length + elements.length / 2];
        //копируем элементы
        for (int i = 0; i < count; i++) {
            newElements[i] = elements[i];
        }
        //заменяем ссылку на новый массив
        elements = newElements;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }
}

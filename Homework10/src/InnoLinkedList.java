
/**
 * 14.03.2021
 * Homework10
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class InnoLinkedList implements InnoList {
    // Создается внутренний класс узлов
    private static class Node {
        int value;
        Node next;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

        Node(int value) {
            this.value = value;
        }

    }

    private Node first;

    private Node last;

    private int count;

    /**
     * Метод вывода всех узлов
     */
    public void print() {
        System.out.print(first.value + " ");
        Node current = first;
        for (int i = 0; i < count - 1; i++) {
            if (current.next == null) {
                break;
            }
            current = current.next;
            System.out.print(current.value + " ");

        }
        System.out.println();
    }

    /**
     * Метод удаления конкретного значения
     *
     * @param element удаляемый элемент
     */
    @Override
    public void remove(int element) {
        // Создаются временные узлы, равный первому и пустой
        Node temp = first, prev = null;

        // Если первый узел содержит удаляемый элемент, то изменяем его
        if (temp != null && temp.value == element) {
            first = temp.next;
            return;
        }

        // Цикл работает, до момента нахождения элемента или окончания всех узлов
        // Во время него приравниваем значения узлов prev к temp
        // И дальше проходимся по элементам temp
        while (temp != null && temp.value != element) {
            prev = temp;
            temp = temp.next;
        }

        // Если элемент был не найден
        if (temp == null)
            return;

        // Если был найден, то выражение "перескакивает" через удаляемый элемент
        prev.next = temp.next;
    }


    /**
     * Метод определения наличия значения в узлах
     *
     * @param element искомый элемент
     * @return
     */
    @Override
    public boolean contains(int element) {
        //Создаем новые узлы и проходимся по каждому их них
        Node current = first;
        for (int i = 0; i < count - 1; i++) {
            current = current.next;
            if (first.value == element) {
                return true;
            }
            if (current.value == element) {
                return true;
            }
        }
        return false;
    }

    /**
     * Метод добавления значения в узел по индексу
     *
     * @param index   куда хотим вставить элемент
     * @param element вставляемый элемент
     */
    @Override
    public void insert(int index, int element) {
        //Создаем новый узел
        Node node = new Node(element);
        node.next = null;

        if (first == null) {
            //Если первый узел отсутсвует и вставляемый индекс не равен нулю, тогда выходим
            if (index != 0) {
                return;
                // В противном случае делаем вставляемый элемент первым
            } else {
                first = node;
            }
        }
        //Если есть в наличии первый узел и вставляемый индекс = 0
        // Тогда смещаем изначальное представление узлов и вставляем нужный элемент в начало
        if (first != null && index == 0) {
            node.next = first;
            first = node;
            return;
        }
        // Создаем 2 узла - "текущий", равный первому и "предыдущий" - пустой
        Node current = first;
        Node previous = null;

        int i = 0;
        // Заполнияем индексы узла "previous" значениями из "current" до нужного нам индекса
        while (i < index) {
            previous = current;
            current = current.next;

            if (current == null) {
                break;
            }
            i++;
        }
        //Вставляем созданный нами узел с конкретным значением
        node.next = current;
        previous.next = node;
        count++;
    }

    /**
     * Метод добавления в начало
     *
     * @param element
     */
    @Override
    public void addToBegin(int element) {
        //Создаем новый узел, приравниваем у него ссылку на следующий, как в изначальных узлах
        //Приравниваем изначальный узел к новому узлу
        Node new_node = new Node(element);
        new_node.next = first;
        first = new_node;
        count++;
    }

    /**
     * Метод удаления конкретного узла
     *
     * @param index
     */
    @Override
    public void removeByIndex(int index) {
        // Если наш узел - пустой, выходим
        if (first == null)
            return;

        // Создаем временный узел, равный изначальному
        Node temp = first;

        // Если нужно удалить первый индекс
        // То приравниваем изначальный узел к временному, но начиная со второго индекса
        if (index == 0) {
            first = temp.next;
            return;
        }
        // Если удаляемый индекс не первый, то находим узел, который находится до удаляемого
        for (int i = 0; temp != null && i < index - 1; i++)
            temp = temp.next;

        // Если удаляемый индекс больше размера узла
        if (temp == null || temp.next == null)
            return;

        //Создаем новый узел, который указывает на следующий индекс временного узла, в обход удаляемого
        Node next = temp.next.next;

        temp.next = next;
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;
        }
        // новый узел - последний
        last = newNode;
        count++;
    }
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        if (first == null){
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        }
//        else{
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null){
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли до последнего узла
//            //теперь новый узел - самый последний
//            current.next = newNode;
//        }

//        count++;

//    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next;
            }
            return current.value;
        } else {

            return -1;
        }
    }
}

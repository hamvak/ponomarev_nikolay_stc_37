create table player
(
    id     bigserial primary key,
    ip     varchar(20),
    name   varchar(20),
    points bigint,
    wins   bigint,
    loses  bigint
);

create table game
(
    id                  bigserial primary key,
    date                double precision,
    player_first_id     bigint,
    player_second_id    bigint,
    player_first_shots  bigint,
    player_second_shots bigint,
    game_time_amount    double precision,
    foreign key (player_first_id) references player (id),
    foreign key (player_second_id) references player (id)
);

create table shot
(
    id         bigserial primary key,
    date       double precision,
--     is_hit boolean,
    game_id    bigint,
    shooter_id bigint,
    target_id  bigint,
    foreign key (game_id) references game (id),
    foreign key (shooter_id) references player (id),
    foreign key (target_id) references player (id)
);

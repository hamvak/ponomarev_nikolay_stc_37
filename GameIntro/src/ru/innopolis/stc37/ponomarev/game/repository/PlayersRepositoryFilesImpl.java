package ru.innopolis.stc37.ponomarev.game.repository;

import ru.innopolis.stc37.ponomarev.game.models.Player;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * 03.04.2021
 * GameIntro
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private String dbFileName;
    private String sequenceFileName;
    private static BufferedReader reader;
    private static BufferedWriter writer;

    public Map<String, Player> players;

    public PlayersRepositoryFilesImpl(String dbFileName, String sequenceFileName) {
        this.dbFileName = dbFileName;
        this.sequenceFileName = sequenceFileName;
        players = new HashMap<>();
    }

    /**
     * Метод для поиско игрока по его имени
     *
     * @param nickname - имя искомого игрока
     * @return - возвращает игрока из списка игроков, по его имени
     */
    @Override
    public Player findByNickname(String nickname) {
        try {
            reader = new BufferedReader(new FileReader(dbFileName));
            String line = reader.readLine();
            // Разбиваем каждую строку на массив строк
            while (line != null) {
                String[] strings = line.split("#");
                //Создаем игрока по значениям из конкретной строки
                Player player = new Player(strings[5], strings[1], Integer.parseInt(strings[2]), Integer.parseInt(strings[4]),
                        Integer.parseInt(strings[3]));
                player.setId(Long.parseLong(strings[0]));
                //Кладем созданного игрока в уже существующий список игроков
                players.put(player.getName(), player);
                line = reader.readLine();
            }
            reader.close();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return players.get((nickname));
    }

    /**
     * Метод, который записывает нового игрока в файл
     *
     * @param player - новый игрок
     */
    @Override
    public void save(Player player) {
        // Кладем в существующий список нового игрока по его имени
        players.put(player.getName(), player);

        player.setId(generateId());

        //Создаем новый массив
        ArrayList<String> arrayList = new ArrayList<String>();
        String line;

        try {
            //Кладем в созданный массив строки из текстового файла
            reader = new BufferedReader(new FileReader(dbFileName));
            while ((line = reader.readLine()) != null) {
                arrayList.add(line);
            }
            reader.close();
            //записываем нового игрока в список
            arrayList.add(arrayList.size(), player.getId() + "#" + player.getName() + "#" + player.getPoints() + "#"
                    + player.getMaxLosesCount() + "#" + player.getMaxWinsCount() + "#" + player.getIp());
            //Перезаписываем текствовый файл
            Files.write(Paths.get(dbFileName), arrayList, StandardCharsets.UTF_8);

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод обновления игроков в файле players_db.txt
     *
     * @param player - существующий игрок
     */
    @Override
    public void update(Player player) {
        //Кладем существующего игрока в его ячейку "списка" по имени
        players.put(player.getName(), player);
        //Создаем новый массив
        ArrayList<String> arrayList = new ArrayList<String>();
        String line;

        try {
            //Кладем в созданный массив строки из текстового файла
            reader = new BufferedReader(new FileReader(dbFileName));
            while ((line = reader.readLine()) != null) {
                arrayList.add(line);
            }
            reader.close();
            //Ищем по имени игрока нужную нам строчку и меняем её
            for (int i = 0; i < arrayList.size(); i++) {
                if (arrayList.get(i).contains(player.getName())) {
                    arrayList.set(i, player.getId() + "#" + player.getName() + "#" + player.getPoints() + "#"
                            + player.getMaxLosesCount() + "#" + player.getMaxWinsCount() + "#" + player.getIp());
                    break;
                }
            }
            //Перезаписываем текствовый файл
            Files.write(Paths.get(dbFileName), arrayList, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private Long generateId() {
        try {
            reader = new BufferedReader(new FileReader(sequenceFileName));
            // прочитали последний сгенерированный id как строку

            String lastGeneratedIdAsString = reader.readLine();
            // преобразуем ее в Long
            Long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();

            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}



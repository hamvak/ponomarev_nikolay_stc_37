package ru.innopolis.stc37.ponomarev.game.repository;

import ru.innopolis.stc37.ponomarev.game.models.Shot;

import java.util.ArrayList;
import java.util.List;

/**
 * 26.03.2021
 * GameIntro
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class ShotsRepositoryListImpl implements ShotsRepository {

    protected List<Shot> shots;

    public ShotsRepositoryListImpl() {
        shots = new ArrayList<>();
    }

    @Override
    public void save(Shot shot) {
        shots.add(shot);
    }
}


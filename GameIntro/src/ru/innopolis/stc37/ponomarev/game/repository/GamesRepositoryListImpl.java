package ru.innopolis.stc37.ponomarev.game.repository;

import ru.innopolis.stc37.ponomarev.game.models.Game;

import java.util.ArrayList;
import java.util.List;

/**
 * 26.03.2021
 * GameIntro
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class GamesRepositoryListImpl implements GamesRepository {

    protected List<Game> games;

    public GamesRepositoryListImpl() {
        games = new ArrayList<>();
}

    @Override
    public void save(Game game) {
        game.setId((long)games.size());
        games.add(game);
    }

    @Override
    public Game findById(Long gameId) {
        return games.get(gameId.intValue());
    }

    @Override
    public void update(Game game) {
        games.set(game.getId().intValue(), game);
    }


}



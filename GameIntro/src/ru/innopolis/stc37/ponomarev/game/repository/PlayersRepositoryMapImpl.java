package ru.innopolis.stc37.ponomarev.game.repository;

import ru.innopolis.stc37.ponomarev.game.models.Player;

import java.util.HashMap;
import java.util.Map;

/**
 * 26.03.2021
 * GameIntro
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class PlayersRepositoryMapImpl implements PlayersRepository {
    public Map<String, Player> players;

    public PlayersRepositoryMapImpl() {
        players = new HashMap<>();
    }

    @Override
    public Player findByNickname(String nickname) {
        return players.get((nickname));
    }

    @Override
    public void save(Player player) {
        players.put(player.getName(), player);
    }

    @Override
    public void update(Player player) {
        if (players.containsKey(player.getName())){
            players.put(player.getName(), player);
        }
        else{
            System.err.println("Updated player dont exists");
        }
    }


}




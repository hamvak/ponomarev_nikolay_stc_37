package ru.innopolis.stc37.ponomarev.game.app;

import ru.innopolis.stc37.ponomarev.game.dto.StatisticDto;
import ru.innopolis.stc37.ponomarev.game.repository.*;
import ru.innopolis.stc37.ponomarev.game.service.GameService;
import ru.innopolis.stc37.ponomarev.game.service.GameServiceImpl;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl("players_db.txt", "players_sequence.txt");
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl("shots_db.txt", "shots_sequence.txt");
        GameService gameService = new GameServiceImpl(gamesRepository, playersRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        for (int j = 0; j < 3; j++) {

            System.out.println("Enter players");

            String first = scanner.nextLine();
            String second = scanner.nextLine();
            Random random = new Random();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;
            int i = 0;

            while (i < 4) {

                System.out.println(shooter + " take shot at " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Get hit");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Miss");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;

            }
            StatisticDto statistic = gameService.finishGame(gameId);
            System.out.println(statistic);

        }


    }
}





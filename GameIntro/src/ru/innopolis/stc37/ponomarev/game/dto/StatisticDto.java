package ru.innopolis.stc37.ponomarev.game.dto;

/**
 * 26.03.2021
 * GameIntro
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
// информация об игре
public class StatisticDto {
    private Long gameId;
    private String playerFirst;
    private String playerSecond;
    private long time;
    private int hitsCountFirst;
    private int hitsCountSecond;
    private int pointsCountFirst;
    private int pointsCountSecond;
    private String victoryLine;

    public StatisticDto() {

    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("");
        sb.append(" Game id = ").append(gameId);
        sb.append("\n Player 1: ").append(playerFirst);
        sb.append(", hits - ").append(hitsCountFirst);
        sb.append(", points - ").append(pointsCountFirst);
        sb.append("\n Player 2: ").append(playerSecond);
        sb.append(", hits - ").append(hitsCountSecond);
        sb.append(", points - ").append(pointsCountSecond);
        sb.append("\n Victory : ").append(victoryLine);
        sb.append("\n Game lasts: ").append(time).append(" seconds");
        return sb.toString();
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getPlayerFirst() {
        return playerFirst;
    }

    public void setPlayerFirst(String playerFirst) {

        this.playerFirst = playerFirst;
    }

    public String getPlayerSecond() {
        return playerSecond;
    }

    public void setPlayerSecond(String playerSecond) {
        this.playerSecond = playerSecond;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public int getHitsCountFirst() {
        return hitsCountFirst;
    }

    public void setHitsCountFirst(int hitsCountFirst) {
        this.hitsCountFirst = hitsCountFirst;
    }

    public int getHitsCountSecond() {
        return hitsCountSecond;
    }

    public void setHitsCountSecond(int hitsCountSecond) {
        this.hitsCountSecond = hitsCountSecond;
    }

    public int getPointsCountFirst() {
        return pointsCountFirst;
    }

    public int getPointsCountSecond() {
        return pointsCountSecond;
    }

    public void setPointsCountFirst(int pointsCountFirst) {
        this.pointsCountFirst = pointsCountFirst;
    }

    public void setPointsCountSecond(int pointsCountSecond) {
        this.pointsCountSecond = pointsCountSecond;
    }

    public String getVictoryLine() {
        return victoryLine;
    }

    public void setVictoryLine(String victoryLine) {
        this.victoryLine = victoryLine;
    }
}





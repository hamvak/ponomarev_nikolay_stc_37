package ru.innopolis.stc37.ponomarev.game.service;

import ru.innopolis.stc37.ponomarev.game.dto.StatisticDto;
import ru.innopolis.stc37.ponomarev.game.models.Game;
import ru.innopolis.stc37.ponomarev.game.models.Player;
import ru.innopolis.stc37.ponomarev.game.models.Shot;
import ru.innopolis.stc37.ponomarev.game.repository.GamesRepository;
import ru.innopolis.stc37.ponomarev.game.repository.PlayersRepository;
import ru.innopolis.stc37.ponomarev.game.repository.ShotsRepository;

import java.time.LocalDateTime;

/**
 * 26.03.2021
 * GameIntro
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class GameServiceImpl implements GameService {
    private GamesRepository gamesRepository;
    private PlayersRepository playersRepository;
    private ShotsRepository shotsRepository;

    public GameServiceImpl(GamesRepository gamesRepository, PlayersRepository playersRepository, ShotsRepository shotsRepository) {
        this.gamesRepository = gamesRepository;
        this.playersRepository = playersRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);

        game.setSecondsGameTimeAmount(System.currentTimeMillis());

        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет первого игрока под таким именем
        if (player == null) {
            // создаем игрока
            player = new Player(ip, nickname, 0, 0, 0);
            // сохраняем
            playersRepository.save(player);
        } else {
            // если такой игрок был, то обновляем АйПи адрес
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого  стреляли из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);
        // увеличиваем очки у стрелявшего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стрелявший - второй игрок
        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            // сохраняем информацию о выстреле
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);

    }

    /**
     * Метод, который выводит статистику по завершенной игре
     *
     * @param gameId - ID игры
     * @return - возвращает статистику
     */
    @Override
    public StatisticDto finishGame(Long gameId) {
        final int BASE_ONE = 1;
        Game game = gamesRepository.findById(gameId);
        StatisticDto statistic = new StatisticDto();
        Player playerFirst = game.getPlayerFirst();
        Player playerSecond = game.getPlayerSecond();
        statistic.setGameId(gameId);
        statistic.setPlayerFirst(playerFirst.getName());
        statistic.setPlayerSecond(playerSecond.getName());
        statistic.setHitsCountFirst(game.getPlayerFirstShotsCount());
        statistic.setHitsCountSecond(game.getPlayerSecondShotsCount());
        statistic.setPointsCountFirst(statistic.getPointsCountFirst() + playerFirst.getPoints());
        statistic.setPointsCountSecond(statistic.getPointsCountSecond() + playerSecond.getPoints());


        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) {
            playerFirst.setMaxWinsCount(playerFirst.getMaxWinsCount() + BASE_ONE);
            playerSecond.setMaxLosesCount(playerSecond.getMaxLosesCount() + BASE_ONE);
            statistic.setVictoryLine(playerFirst.getName());
        }

        if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()) {
            playerSecond.setMaxWinsCount(playerSecond.getMaxWinsCount() + BASE_ONE);
            playerFirst.setMaxLosesCount(playerFirst.getMaxLosesCount() + BASE_ONE);
            statistic.setVictoryLine(playerSecond.getName());
        } else if (game.getPlayerFirstShotsCount().equals(game.getPlayerSecondShotsCount())) {
            statistic.setVictoryLine(" Friendship");
        }

        playerFirst.setPoints(statistic.getPointsCountFirst() + statistic.getHitsCountFirst());
        playerSecond.setPoints(statistic.getPointsCountSecond() + statistic.getHitsCountSecond());
        playersRepository.update(playerFirst);
        playersRepository.update(playerSecond);
        statistic.setTime((System.currentTimeMillis() - game.getSecondsGameTimeAmount()) / 1000);
        return statistic;

    }
}




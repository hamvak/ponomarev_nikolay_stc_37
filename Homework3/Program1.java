import java.util.Arrays;
import java.util.Scanner;

class Program1{
			
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		// Пользователь выбирает одну из 6-ти опций, которую ему нужна
		System.out.println("Choose option :");
		System.out.println("1. arraySum ");
		System.out.println("2. reversedArray ");
		System.out.println("3. arrayAverage ");
		System.out.println("4. arraySwitchMinMax ");
		System.out.println("5. bubbleSortArray ");
		System.out.println("6. arrayToNumber ");
		// Ввод размера массива и чисел внутри него
		int option = scanner.nextInt();
		System.out.println("Enter array size ");
		int size = scanner.nextInt();
		int array [] = new int [size];
		System.out.println("Enter numbers: ");
		for (int i= 0; i < array.length; i++){
			array[i] = scanner.nextInt();
			}
		// Вывод изначального массива
		System.out.println(Arrays.toString(array));	
		// В зависимости от номера выбранной опции, выполняется одно из условий	
			if (option == 1){
				arraySum(array);
				System.out.println("arraySum = " + array);
			}
			else if (option == 2){
				reversedArray (array);
				System.out.println("reversedArray = " + Arrays.toString(array));
			}
			else if (option == 3){
				arrayAverage(array);
				System.out.println("arrayAverage = " + array);
				
			}
			else if (option == 4){
				arraySwitchMinMax(array);
				System.out.println("arraySwitchMinMax = " + Arrays.toString(array));
				
			}
			else if (option == 5){
				bubbleSortArray (array);
				System.out.println("bubbleSortArray = " + Arrays.toString(array));
				
			}
			// Процедура перевода массива в число
			else if (option == 6){
				int arrayToNumber = arrayToNumber(array);	
				System.out.println("arrayToNumber = " + arrayToNumber );			
		   	}			
		   	else{
			System.out.println("Incorrect option ");
			}										
	}
 		// Функция вывода суммы элементов массива
		public static int arraySum (int array[]){
		
			int arraySum = 0;

			for (int i= 0; i < array.length; i++){
				arraySum += array[i];
			}
			return arraySum;
		}
		// Функция разворота массива
 		public static int [] reversedArray ( int array[]){

 			int reversedArray[] = new int[array.length];
			int j = 0;
			for (int i = array.length -1; i >= 0; i--) {
				reversedArray[j++] = array[i];
 			}
 			return reversedArray;
 		}
 		// Функция среднего арифметического массива
 		public static double arrayAverage (int array[]){
 			double arrayAverage = 0;
			
			for (int i = 0; i < array.length; i++) {
			arrayAverage += array[i];
			}
			arrayAverage /= array.length;
			return arrayAverage;
 		}
 		// Функция перемены местами максимального и минимального элемента массива
 		public static int [] arraySwitchMinMax (int array[]){
 	
			int min = array [0];
			int max = array [0];
			int positionOfMin = 0;
			int positionOfMax = 0;
			
			for (int i = 0; i < array.length; i++){
				if (array[i] < min){
					min = array[i];
					positionOfMin = i;
				}
				if (array[i]>max){
					max = array[i];
					positionOfMax = i;
				}
			}
			int swap = array[positionOfMin];
		    array[positionOfMin] = array[positionOfMax];
		    array[positionOfMax] = swap;
				 	   
			return array;
 		}
 		// Функция сортировки методом пузырька
 		public static int [] bubbleSortArray (int array[]){

 			for (int i=0; i<array.length -1; i++){
				for (int j = array.length -1; j > i; j--){
					if(array[j-1]>array[j]){
						int temp = array[j - 1];
						array [j - 1] = array [j];
						array[j] = temp;	
					}
				}
			}
			return array;
 		}
 		// Функция перевода массива в число
 		public static int arrayToNumber (int array []){
		
		int arrayToNumber = 0;
		for (int i= 0; i<array.length; i++){
			 arrayToNumber = arrayToNumber * 10 + array[i];					
		}
		return arrayToNumber;

	}
}


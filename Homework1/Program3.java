import java.util.Scanner;

class Program3{
	public static void main(String[] args) {
		final int BASE = 10;
		int product = 1;
		Scanner scanner = new Scanner(System.in);
		int currentNumber = scanner.nextInt();

		while (currentNumber !=0){
			int digitsSum = 0;
			int copyNumber = currentNumber;
			while (copyNumber != 0){
				digitsSum += copyNumber % BASE;
				copyNumber /= BASE;
			}
			boolean isPrime = true;
			for (int i = 2; i < digitsSum; i++){
				if (digitsSum % i == 0){
					isPrime = false;
					break;
				}
			}
			if (isPrime){
				product *= currentNumber;
			}
			currentNumber = scanner.nextInt();
		}
		System.out.println(product);
	}
}

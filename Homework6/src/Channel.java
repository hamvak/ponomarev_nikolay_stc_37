import java.util.Random;
public class Channel {
    private String[] channelName;

    public Channel(String[] channelName) {
        this.channelName = channelName;
    }
    // Функция выбора случайного элемента из массива программ
    public static String programGenerate(String[] programKit) {
        Random random = new Random();
        int rnd = random.nextInt(programKit.length);
        return programKit[rnd];
    }

    public String[] getChannelName() {
        return channelName;
    }
}


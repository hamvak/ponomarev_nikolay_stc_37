import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Please enter channel number");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        //Массивы с программами для каждого канала
        String[] programKit = {"The Voice",
                "Top Gear",
                "Britain's Got Talent", "MasterChef",
                "MythBusters",
                "Hell's Kitchen", "Pimp my Ride",
                "Late Night with Jimmy Fallon",
                "Muppets Tonight"};
        //Массив каналов
        String[] channels = {"BBC", "ABC", "CNN"};
        //Инициализация каждого объекта
        Program programs = new Program(programKit);

        Channel channel = new Channel(channels);
        TV tv = new TV();
        RemoteController remoteController = new RemoteController();
        tv.setChannel(channel);
        remoteController.setTv(tv);
        // В зависимости от введеного номера канала выбирает определенное значение из массива каналов
        // И генерирует случайную программу из пакета программ для конкретного канала
        remoteController.switchChannel(n, channel.getChannelName(), programs.getProgramName());
    }
}


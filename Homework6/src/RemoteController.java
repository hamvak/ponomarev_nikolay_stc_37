public class RemoteController {
    private TV tv;

    // Функция "переключения канала". Вызывает функцию "выбор канала" из класса TV
    // Для каждого канала создается отдельный массив программ, в соотношении 1 к 3
    // И группируются программы по порядку(первые 3 программы для первого канала, вторые 3 программы для второго канала...)
    public void switchChannel(int n, String[] channels, String[] programKits) {
        int i = 0;
        int k;
        String[] programs = new String[3];
        switch (n) {
            case 1:
                for (i = 0; i < programKits.length - 6; i++) {
                    programs[i] = programKits[i];
                }
                tv.chooseChannel(n, channels, programs);
                break;
            case 2:
                for (k = 3; k < programKits.length - 3; k++) {
                    programs[i] = programKits[k];
                    i++;
                }
                tv.chooseChannel(n, channels, programs);
                break;
            case 3:
                for (k = 6; k < programKits.length; k++) {
                    programs[i] = programKits[k];
                    i++;
                }
                tv.chooseChannel(n, channels, programs);
                break;
            default:
                System.err.println("Invalid channel, please enter number from 1 to 3 ");
        }
    }
    public void setTv(TV tv) {
        this.tv = tv;
    }

}

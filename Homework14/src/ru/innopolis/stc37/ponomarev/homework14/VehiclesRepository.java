package ru.innopolis.stc37.ponomarev.homework14;

import java.util.List;

/**
 * 07.04.2021
 * Homework14
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public interface VehiclesRepository {
    /**
     * Метод показывает весь транспорт, что находится в файле
     *
     * @return - транспорт
     */
    List<Vehicle> findAll();

    /**
     * Метод показывает весь траспорт с искомым цветом
     *
     * @param color - цвет транспорта
     * @return - транспорт
     */
    List<Vehicle> findAllByColorAndMileage(String color);

    /**
     * Метод показывает весь транспорт с искомым пробегом
     *
     * @param mileage - пробег транспорта
     * @return - транспорт
     */
    List<Vehicle> findAllByColorAndMileage(Long mileage);

    /**
     * Метод показывает весь транспорт с ценой в заданном диапазоне
     *
     * @param from - начальная цена
     * @param to   - конечная цена
     * @return - транспорт
     */
    List<Vehicle> findAllByValue(Long from, Long to);

    /**
     * Метод показывает транспорт с минимальной стоимостью
     *
     * @return - транспорт
     */
    List<Vehicle> findByMinValue();

    /**
     * Метод показывает среднюю цену у конкретной модели транспорта
     *
     * @param vehicleName - модель транспорта
     * @return - транспорт
     */
    List<Vehicle> findAverageValue(String vehicleName);

}


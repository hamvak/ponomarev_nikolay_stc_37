package ru.innopolis.stc37.ponomarev.homework14;

public class Main {

    public static void main(String[] args) {

        VehiclesRepository vehiclesRepository = new VehiclesRepositoryImpl("vehicles_db.txt");

        System.out.println(vehiclesRepository.findAll());
        System.out.println(vehiclesRepository.findAllByColorAndMileage("Black"));
        System.out.println(vehiclesRepository.findAllByColorAndMileage(0L));
        System.out.println(vehiclesRepository.findAllByValue(700000L, 800000L));
        System.out.println(vehiclesRepository.findByMinValue());
        System.out.println(vehiclesRepository.findAverageValue("Camry"));
    }
}


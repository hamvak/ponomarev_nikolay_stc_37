package ru.innopolis.stc37.ponomarev.homework14;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 07.04.2021
 * Homework14
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class VehiclesRepositoryImpl implements VehiclesRepository {

    private String fileName;

    public VehiclesRepositoryImpl(String fileName) {
        this.fileName = fileName;
    }


    private static final Function<String, Vehicle> vehicleMapper = line -> {
        String parsedLine[] = line.split("#");
        return new Vehicle(Long.parseLong(parsedLine[0]), parsedLine[1],
                parsedLine[2], Long.parseLong(parsedLine[3]),
                Long.parseLong(parsedLine[4]));

    };

    /**
     * Метод показывает весь транспорт, что находится в файле
     *
     * @return - транспорт
     */
    @Override
    public List<Vehicle> findAll() {
        System.out.println("All vehicles");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Vehicle> vehicles = reader.lines()
                    .map(vehicleMapper)
                    .collect(Collectors.toList());
            reader.close();
            return vehicles;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод показывает весь траспорт с искомым цветом
     *
     * @param color - цвет транспорта
     * @return - транспорт
     */
    @Override
    public List<Vehicle> findAllByColorAndMileage(String color) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Vehicle> vehicles = reader.lines()
                    .map(vehicleMapper)
                    .filter(vehicle -> vehicle.getColorName().equals(color))
                    .collect(Collectors.toList());
            List<Long> ids = vehicles.stream().map(Vehicle::getId).collect(Collectors.toList());
            System.out.println("Vehicles Ids with color(" + color + ")" + " = " + ids);
            reader.close();
            return vehicles;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод показывает весь транспорт с искомым пробегом
     *
     * @param mileage - пробег транспорта
     * @return - транспорт
     */
    @Override
    public List<Vehicle> findAllByColorAndMileage(Long mileage) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Vehicle> vehicles = reader.lines()
                    .map(vehicleMapper)
                    .filter(vehicle -> vehicle.getMileage().equals(mileage))
                    .collect(Collectors.toList());
            List<Long> ids = vehicles.stream().map(Vehicle::getId).collect(Collectors.toList());
            System.out.println("Vehicles Ids with mileage (" + mileage + ")" + " = " + ids);
            reader.close();
            return vehicles;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод показывает весь транспорт с ценой в заданном диапазоне
     *
     * @param from - начальная цена
     * @param to   - конечная цена
     * @return - транспорт
     */
    @Override
    public List<Vehicle> findAllByValue(Long from, Long to) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Vehicle> vehicles = reader.lines()
                    .map(vehicleMapper)
                    .distinct()
                    .filter(vehicle -> vehicle.getValue() >= from)
                    .filter(vehicle -> vehicle.getValue() <= to)
                    .collect(Collectors.toList());
            int size = vehicles.size();
            System.out.println("Number of vehicles with value from " + from + ", to " + to + " = " + size);
            reader.close();
            return vehicles;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод показывает транспорт с минимальной стоимостью
     *
     * @return - транспорт
     */
    @Override
    public List<Vehicle> findByMinValue() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Vehicle> vehicles = reader.lines()
                    .map(vehicleMapper)
                    .min(Comparator.comparing(Vehicle::getValue))
                    .stream().collect(Collectors.toList());
            List<String> color = vehicles.stream().map(Vehicle::getColorName).collect(Collectors.toList());
            System.out.println("Color of vehicle with min value = " + color);
            reader.close();
            return vehicles;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Метод показывает среднюю цену у конкретной модели транспорта
     *
     * @param vehicleName - модель транспорта
     * @return - транспорт
     */
    @Override
    public List<Vehicle> findAverageValue(String vehicleName) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            List<Vehicle> vehicles = reader.lines()
                    .map(vehicleMapper)
                    .filter(vehicle -> vehicle.getModelName().equals(vehicleName))
                    .collect(Collectors.toList());
            double average = vehicles.stream().collect(Collectors.averagingDouble(Vehicle::getValue));
            System.out.println("Average price of " + vehicleName + " = " + average);
            reader.close();
            return vehicles;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}


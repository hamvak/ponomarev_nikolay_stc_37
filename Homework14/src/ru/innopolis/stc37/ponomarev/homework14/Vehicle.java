package ru.innopolis.stc37.ponomarev.homework14;

import java.util.Objects;

/**
 * 07.04.2021
 * Homework14
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Vehicle {
    private Long id;
    private String modelName;
    private String colorName;
    private Long mileage;
    private Long value;

    public Vehicle(Long id, String modelName, String colorName, Long mileage, Long value) {
        this.id = id;
        this.modelName = modelName;
        this.colorName = colorName;
        this.mileage = mileage;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return Objects.equals(id, vehicle.id) && Objects.equals(modelName, vehicle.modelName) && Objects.equals(colorName, vehicle.colorName) && Objects.equals(mileage, vehicle.mileage) && Objects.equals(value, vehicle.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, modelName, colorName, mileage, value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Vehicle{");
        sb.append("id=").append(id);
        sb.append(", modelName='").append(modelName).append('\'');
        sb.append(", colorName='").append(colorName).append('\'');
        sb.append(", mileage=").append(mileage);
        sb.append(", value=").append(value);
        sb.append('}');
        return sb.toString();
    }
}


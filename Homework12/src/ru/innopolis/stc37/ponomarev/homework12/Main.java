package ru.innopolis.stc37.ponomarev.homework12;

public class Main {

    public static void main(String[] args) {
        Map<String, String> map = new HashMapImpl<>();

        map.put("Даниил", "Вдовинов");// index - 10, nodes - 1
        map.put("Виктор", "Евлампьев"); // index - 0, nodes - 1
        map.put("Nikolay", "Пономарев"); // index - 5, nodes - 2 (djamil)
        map.put("Hash", "Богомолов"); // index - 14, nodes - 2 (table)
        map.put("Teheran", "HELLO2");// index - 1, nodes - 3 (siblings, марсель)
        map.put("Марсель", "Сидиков"); // index - 1, при внесении индекс совпал с "teheran"
        map.put("Table", "Мухутдинов");// index - 14, при внесении индекс совпал с "hash"
        map.put("Djamil", "Садыков");// index - 5, при внесении индекс совпал с "nikolay"
        map.put("Siblings", "HELLO1"); // index - 1, при внесении индекс совпал с "teheran"


        System.out.println(map.get("Даниил"));
        System.out.println(map.get("Виктор"));
        System.out.println(map.get("Nikolay"));
        System.out.println(map.get("Hash"));
        System.out.println(map.get("Teheran"));
        System.out.println(map.get("Марсель"));
        System.out.println(map.get("Table"));
        System.out.println(map.get("Djamil"));
        System.out.println(map.get("Siblings"));


    }
}




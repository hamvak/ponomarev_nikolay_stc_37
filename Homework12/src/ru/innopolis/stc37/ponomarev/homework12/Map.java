package ru.innopolis.stc37.ponomarev.homework12;

/**
 * 24.03.2021
 * Homework11
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public interface Map<K, V> {
    // положить значение value под ключом key
    // a[5] = 7 <-> map.put(5, 7);
    void put(K key, V value);

    // получить значение по ключу
    // int i = a[5] <-> int i = map.get(5);
    V get(K key);
}






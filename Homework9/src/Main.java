import java.util.Locale;

public class Main {

    public static void main(String[] args) {

        // Инициализация массивов строк и чисел для каждой реализации отдельно

        NumbersAndStringProcessor string1 = new NumbersAndStringProcessor(new String[]{"string", "processor"});
        NumbersAndStringProcessor string2 = new NumbersAndStringProcessor(new String[]{"@S1t2r3i4N5g2}", "!p123rocess146o&r"});
        NumbersAndStringProcessor string3 = new NumbersAndStringProcessor(new String[]{"string", "processor"});

        NumbersAndStringProcessor number1 = new NumbersAndStringProcessor(new int[]{1234, 452});
        NumbersAndStringProcessor number2 = new NumbersAndStringProcessor(new int[]{10203040, 405020});
        NumbersAndStringProcessor number3 = new NumbersAndStringProcessor(new int[]{1234567, 3579});
         // Лямбда - реализация разворота числа
        NumberProcess numberProcessReverse = Main::reverseNumber;
        //Лямбда - реализация удаления нулей из числа
        NumberProcess numberProcessRemoveZero = Main::removeZero;
        //Лямбда - реализация замены нечетной цифры на ближайшую четную снизу
        NumberProcess numberProcessEven = Main::numberEven;

         //Лямбда - реализация разворота строки
        StringProcess stringProcessReverse = strings -> {
            for (int i = 0; i < strings.length; i++) {
                strings[i] = new StringBuilder(strings[i]).reverse().toString();
            }
            return strings;
        };

         //Лямбда - реализация удаления всех цифр из строк

        StringProcess stringProcessRemoveDigits = strings -> {

            for (int i = 0; i < strings.length; i++) {

                strings[i] = strings[i].replaceAll("[,0-9]", "");
            }
            return strings;
        };


         //Лямбда - реализация перевода из маленьких букв в БОЛЬШИЕ
        StringProcess stringProcessUpperCase = strings -> {
            for (int i = 0; i < strings.length; i++) {
                strings[i] = strings[i].toUpperCase(Locale.ROOT);
            }
            return strings;
        };
        //Инициализация метода process для каждого объекта
        number1.process(numberProcessReverse);
        number2.process(numberProcessRemoveZero);
        number3.process(numberProcessEven);

        string1.process(stringProcessReverse);
        string2.process(stringProcessRemoveDigits);
        string3.process(stringProcessUpperCase);
        // Вызов метода печати для каждого объекта
        NumbersAndStringProcessor strings[] = {string1, string2, string3};
        NumbersAndStringProcessor numbers[] = {number1, number2, number3};

        for (int i = 0; i < strings.length; i++) {
            strings[i].showProcessedStrings();
        }

        for (int i = 0; i < numbers.length; i++) {
            numbers[i].showProcessedNumbers();
        }
    }

    /**
     * Метод разворота числа
     * Каждое число из  массива объекта number1 возвращается задом наперед
     * @param numbers массив чисел
     * @return вовзращает перевернутое число
     */
    private static int[] reverseNumber(int[] numbers) {
        int[] result = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            while (numbers[i] > 0) {
                result[i] = (result[i] * 10) + (numbers[i] % 10);
                numbers[i] /= 10;
            }
        }
        return result;
    }

    /**
     * Метод удаления нулей из числа
     * reversedArray - аналогично первой реализации является перевернутым массивом чисел объекта number2
     * Во время каждой итерации цикла переворота текущее число, с которым работает цикл
     * Сравнивает с условиями внутри этого цикла и в соответствии с этим проводит операции над текущим числом
     * На выходе получается перевернутый массив, как в прошлой реализации, но уже обработанный по условиям задачи
     * Последний цикл переворачивает получившееся число обратно, для соответствия порядку
     * постановке цифр в изначальном числе
     * @param numbers массив чисел
     * @return возвращает числа без нулей
     */
    private static int[] removeZero(int[] numbers) {
        int[] reversedArray = new int[numbers.length];
        int[] result = new int[numbers.length];
        int[] finalArray = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            //Цикл переворачивает массив
            while (numbers[i] > 0) {
                reversedArray[i] = (reversedArray[i] * 10) + (numbers[i] % 10);
                numbers[i] /= 10;
                //Цикл проверки каждой цифры на то, является ли она нулем
                while (reversedArray[i] > 0) {
                    if (reversedArray[i] % 10 != 0) {
                        result[i] = (result[i] * 10) + reversedArray[i];
                    }
                        reversedArray[i] /= 10;
                }
            }
            //Цикл снова переворачивает масссив
            while (result[i] > 0) {
                finalArray[i] = (finalArray[i] * 10) + (result[i] % 10);
                result[i] /= 10;
            }
        }
        return finalArray;
    }

    /**
     * Метод замены нечетной цифры в числе, на четную снизу
     *  reversedArray - аналогично первой реализации является перевернутым массивом чисел объекта number3
     * Во время каждой итерации цикла переворота текущее число, с которым работает цикл
     * Сравнивает с условиями внутри этого цикла и в соответствии с этим проводит операции над текущим числом
     * На выходе получается перевернутый массив, как в первой реализации, но уже обработанный по условиям задачи
     * Последний цикл переворачивает получившееся число обратно, для соответствия порядку
     *  Постановке цифр в изначальном числе
     * @param numbers массив чисел
     * @return возвращает число с четными цифрами, если были нечетные
     */
    private static int[] numberEven(int[] numbers) {
        int[] reversedArray = new int[numbers.length];
        int[] result = new int[numbers.length];
        int[] finalArray = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            //Цикл переворота массива
            while (numbers[i] > 0) {
                reversedArray[i] = (reversedArray[i] * 10) + (numbers[i] % 10);
                numbers[i] /= 10;
                //Цикл проверки цифры на то, является ли она четным
                while (reversedArray[i] > 0) {
                    if (reversedArray[i] % 2 != 0) {
                        result[i] = (result[i] * 10) + (reversedArray[i] - 1);
                    } else {
                        result[i] = (result[i] * 10) + reversedArray[i];
                    }
                    reversedArray[i] /= 10;
                }
            }
            //Цикл снова переворачивает массив
            while (result[i] > 0) {
                finalArray[i] = (finalArray[i] * 10) + (result[i] % 10);
                result[i] /= 10;
            }
        }
        return finalArray;
    }
}


/**
 * 09.03.2021
 * Homework9
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class NumbersAndStringProcessor {

    private int[] processedNumbers;
    private String[] processedStrings;

    public NumbersAndStringProcessor(int[] processedNumbers) {
        this.processedNumbers = processedNumbers;
    }

    public NumbersAndStringProcessor(String[] processedStrings) {
        this.processedStrings = processedStrings;
    }

    //Метод возвращает измененный массив processedStrings в соответствии с лямбда - реализацией в main
    public String[] process(StringProcess process) {
        String [] result = process.process(processedStrings);
        return processedStrings = result;
    }

    //Метод печати для массива processNumbers
    public void showProcessedStrings() {
        for (int i = 0; i < processedStrings.length; i++){
            System.out.print(processedStrings[i] + " ");
        }
        System.out.println();
    }

    //Метод возвращает измененный массив processedNumbers в соответствии с лямбда - реализацией в main
    public int[] process(NumberProcess process) {
        int [] result = process.process(processedNumbers);
        return processedNumbers = result;
    }

    //Метод печати для массива processNumbers
    public void showProcessedNumbers() {
        for (int i = 0; i < processedNumbers.length; i++) {
            System.out.print(processedNumbers[i] + " ");
        }
        System.out.println();
    }
}


/**
 * 09.03.2021
 * Homework9
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
@FunctionalInterface
public interface NumberProcess {
    int[] process(int[] number);
}

import java.util.Scanner;

class Program2 {
    // Функция нахождения искомого числа бинарным поиском
    // right  - правая граница ( изначально 0 )
    // left  - левая граница (изначально array.length - 1)
    public static int binarySearch(int[] array, int numberForSearch, int right, int left) {
        //Условие для предотвращения выхода за границу массива
        if (right <= left) {
            //Условие нахождения середины (middle)
            int middle = (right + left) / 2;
            // Если элемент массива под индексом "middle" меньше искомого числа
            if (array[middle] > numberForSearch) {
                //Возвращается та же функция, но с уменьшенным значением left
                return binarySearch(array, numberForSearch, right, middle - 1);
            }
            // Если элемент массива под индексом "middle" больше искомого числа
            else if (array[middle] < numberForSearch) {
                //Возвращается та же функция, но с увеличенным значением right
                return binarySearch(array, numberForSearch, middle + 1, left);
            }
            // Если элемент массива под индексом "middle" равен искомому числу
            else if (array[middle] == numberForSearch) {
                // Возвращается значение элемента массива под индексом "middle"
                return array[middle];
            }
        }
        //Если произошел выход за границы массива, возвращается минус 1
        return -1;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int array[] = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};
        int numberForSearch = scanner.nextInt();
        // Если искомое число выходит за рамки массива, выводится ошибка
        if ((numberForSearch <= 0) || (numberForSearch > 99)) {
            System.err.println("Out of range, numberForSearch can be only > 0 and <= 99 ");
        }
        // Вводим переменную isExists, которая обозначает наличие или отсутствие искомого числа в массиве
        boolean isExists = false;
        // result приравниваем к результату return в функции binarySearch
        // Задаем условие, что right  = 0, а left меньше длинны массива на 1
        int result = binarySearch(array, numberForSearch, 0, array.length - 1);

        if (result == numberForSearch) {
            isExists = true;
        }

        System.out.println(isExists);
    }
}


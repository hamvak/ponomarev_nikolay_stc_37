public class Program3 {

    // Функция вычисления числа Фибоначчи
    public static int fib(int n, int a, int b){

       if (n <= 2){
           return a + b;
       }
       return fib(n - 1, b, a + b );
    }

    public static void main(String[] args) {
        int n = Integer.parseInt(args[0]);
        for (int i = 1; i < n; i++){
            System.out.println(fib(i, 0 , 1));
        }

    }

}

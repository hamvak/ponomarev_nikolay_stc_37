import java.util.Scanner;

class Program1 {
    // Функция проверяет, является ли число степенью двойки
    // Сколько раз мы совершили деление, столько же раз и умножили
    // Для наглядности использую вывод числа на каждом этапе
    public static int RateOfTwo(int n) {
        // Вводим константу
        final int BASE_TWO = 2;
        // Конечное условие, при котором заканчивается деление
        if (n == 1) {
            return 1;
        }

        System.out.println("n = " + n);
        // Рекурсия деления чмсла на 2 и затем умножение на 2, начиная с единицы
        int previous = RateOfTwo(n / BASE_TWO);

        int steps = previous * BASE_TWO;
        System.out.println("n = " + steps);
        return steps;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        // result приравниваем к последнему return steps в функции RateOfTwo
        int result = RateOfTwo(n);
        boolean isNumber = false;

        if (result == n) {
            isNumber = true;
        }
        System.out.println(n + " - " + isNumber);
    }
}

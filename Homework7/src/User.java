public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }
    //Реализация builder через вложенный класс
    public static class Builder {

        private final User user;

        public Builder() {
            user = new User();
        }
        //Методы с возвращающим типом builder для необязательных параметров
        public Builder firstName(String firstName) {
            user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            user.lastName = lastName;
            return this;
        }

        public Builder age(int age) {
            user.age = age;
            return this;
        }

        public Builder isWorker(boolean isWorker) {
            user.isWorker = isWorker;
            return this;
        }
        //Генерация объекта
        public User build() {
            return user;
        }

    }

    private User(){

    }

}

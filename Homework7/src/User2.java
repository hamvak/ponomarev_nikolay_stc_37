public class User2 {
    private String firstName = " ";
    private String lastName = " ";
    private int age = 0;
    private boolean isWorker = false;

    public User2 firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public User2 lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public User2 age(int age) {
        this.age = age;
        return this;
    }

    public User2 isWorker(boolean isWorker) {
        this.isWorker = isWorker;
        return this;
    }

    public void build(){
        System.out.println(firstName + " " + lastName + " " + age + " " + isWorker);
    }
}

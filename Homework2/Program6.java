import java.util.Arrays;

class Program6 {
	public static void main(String args[]) {
	
		int array[] = {4, 2, 3, 5, 7};
		// Вывод изначального массива
		System.out.println(Arrays.toString(array));
		int number = 0; // переменная для превращения массива в число

		// Цикл отвечает за количество проходов и приравнивает number к конкретной ячейке
		for (int i= 0; i<array.length; i++){
			 number = number * 10 + array[i];					
		}
		
		// Вывод сформированного числа	
		System.out.println(number);
	}
}

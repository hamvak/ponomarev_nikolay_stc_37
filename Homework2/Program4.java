import java.util.Arrays;
import java.util.Scanner;

class Program4 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		//Вводим размер массива и значения
		int size = scanner.nextInt();
		int array[] = new int [size];

		// Цикл предотвращения выхода за рамки размерности массива при вводе чисел через консоль	
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}

		// Вывод изначального массива
		System.out.println(Arrays.toString(array));

		int temp = 0; // временная переменная
		int min = array [0]; // переменная минимального числа
		int max = array [0]; // переменная максимального числа
		int positionOfMin = 0; // переменная обозначения позиции минимального числа 
		int positionOfMax = 0; // переменная обозначения позиции максимального числа

		// Цикл нахождения позиции минимального и максимального числа
		for (int i = 0; i < array.length; i++){
			if (array[i] < min){
				min = array[i];
				positionOfMin = i;
			}
			if (array[i]>max){
				max = array[i];
				positionOfMax = i;
			}
		}

		// Перестановка позиций максимального и минимального числа в массиве		
	     temp = array[positionOfMax];
	     array[positionOfMax] = array [positionOfMin];
	     array [positionOfMin] = temp;

	     // Вывод измененного массива		     
	     System.out.println(Arrays.toString(array));
	}		
}

import java.util.Scanner;
class Program7 {
 
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println (" Enter array size: ");
		int size = scanner.nextInt();
		
		int r=0;//переменная "строки"
		int c=0;//переменная "столбцы"
		int spec=0;//переменная, увеличивая которую, уменьшаем кол-во шагов следующего цикла
		int i=0;//значение конкретной ячейки
		int array[][]=new int[size][size];
			for(int z=0;i<size*size-1;z++){	
		
				for(int j=0;j<size-spec;j++){
					array[r][c++]=++i;
				}
					c--;//уменьшаем "с" на один, т.к. при выходе из цикла,- "с" увеличивается на раз больше, чем нужно
					++spec;// уменьшаем кол-во шагов следующего цикла
		
				for(int k=0;k<size-spec;k++){
					array[++r][c]=++i;
				}
				
				for(int l=0;l<size-spec;l++){
					array[r][--c]=++i;
				}
				++spec;// уменьшаем кол-во шагов следующего цикла
		
				for(int m=0;m<size-spec;m++){
					array[--r][c]=++i;
				}
				c++;//увеличиваем "с" на один, для "постановки" 1-го цикла на нужный столбец
			}
		
		for(int d=0;d<size;d++){
			for(int s=0;s<size;s++){
				System.out.print(array[d][s] + "\t");
			}

			System.out.println();
		}
	}
}
import java.util.Scanner;

class Program3{
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		// Вводится размер массива и значения
		int size = scanner.nextInt();
		int array[] = new int[size];
		double arrayAverage = 0;// Переменная среднеарифметического

		//Цикл расчета сначала суммы элементов массива, затем среднее арифметическое самого массива
		for (int i = 0; i < array.length; i++) {
			arrayAverage += scanner.nextInt();
		}
		
		arrayAverage /= array.length;

		// Вывод результата на консоль
		System.out.println("arrayAverage = " + arrayAverage);
	}

}
import java.util.Arrays;

class Program5 {
	public static void main(String[] args) {
		int array[] = {3, 2, 10, -5, 15, 18, -50};

		// Вывод изначального массива		
		System.out.println(Arrays.toString(array));

		// Внешний цикл отвечает за номер прохода	
		for (int i=0; i<array.length -1; i++){
			// Внутренний цикл прохода
			// Цикл перебирает значения начиная с последнего и в каждом следующем проходе уменьшает количество элементов.		
			for (int j = array.length -1; j > i; j--){
				if(array[j-1]>array[j]){
					//Обмен значениями
					int temp = array[j - 1];// temp - временная переменная
					array [j - 1] = array [j];
					array[j] = temp;
				}
			}
		}

		// Вывод отсортированного массива
		System.out.println(Arrays.toString(array));				
	}
}
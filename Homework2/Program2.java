import java.util.Arrays;
import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		// Вводится размер массива и значения
		int size = scanner.nextInt();
		int array[] = new int[size];
		int reversedArray[] = new int[array.length]; 
		int j = 0; // Переменная "ячейки" перевернутого массива

		///Цикл для предотвращения выхода за рамки размерности массива, при вводе чисел в консоль
		for (int i = 0; i < array.length; i++) {
			array[i] = scanner.nextInt();
		}
		// Цикл для зеркального отражения массива
		for (int i = array.length -1; i >= 0; i--) {
			reversedArray[j++] = array[i];
		}	

		// Вывод результата на консоль
		System.out.println(Arrays.toString(reversedArray));
	}
}

package Figures;

import AbstractFigures.GeometricFigures;
import Interface.MoveCenterOfFigure;
import Interface.ScaleFigure;

/**
 * 07.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Square extends GeometricFigures implements ScaleFigure, MoveCenterOfFigure {

    protected int length;

    public Square(int length, int x, int y) {
        super(x , y);
        if (length > BASE_ZERO) {
            this.length = length;
        } else {
            System.err.println("Length of RectangularFigure cannot be <=0");
        }
    }

    //Метод предка для расчета площади
    @Override
    public double getArea() {
        return length * length;
    }
    //Метод предка для расчета периметра
    @Override
    public double getPerimeter() {
        return BASE_FOUR * length;
    }

    //Метод перемещения координат центра фигуры
    @Override
    public void moveCenterOfFigure(int value, String message) {
        System.out.printf("Moved center of " +"%s" + " = " + "X(" + "%d" + ") " + ", " + "Y(" + "%d" +  ")\n",message, x + value, y + value  );
    }
    //Метод масштабирования периметра и площади фигуры
    @Override
    public void scale(double value, String message) {
        if (value < BASE_ZERO) {
            System.err.println("Value scale cannot be < 0");
        } else {
            System.out.printf("Scaled perimeter of " + "%s" + " = " + "%.1f \n", message, getPerimeter() * value);
            System.out.printf("Scaled area of " + "%s" + " = " + "%.1f\n", message, getArea() * value);
        }
    }
}




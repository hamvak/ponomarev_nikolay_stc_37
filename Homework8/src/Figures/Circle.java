package Figures;

import AbstractFigures.GeometricFigures;
import Interface.MoveCenterOfFigure;
import Interface.ScaleFigure;

/**
 * 07.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Circle extends GeometricFigures implements ScaleFigure, MoveCenterOfFigure {

    public static final double PI = Math.PI;
    protected double radius;

    public Circle(double radius, int x, int y) {
        super(x, y);
        if (radius > BASE_ZERO) {
            this.radius = radius;
        } else {
            System.err.println("Radius of RoundFigure cannot be <=0");
        }
    }

    //метод предка по расчету площади
    @Override
    public double getArea() {
        return PI * (radius * radius);
    }
    //метод предка по расчету периметра
    @Override
    public double getPerimeter() {
        return BASE_TWO * PI * radius;
    }

    //Метод перемещения координат центра фигуры
    @Override
    public void moveCenterOfFigure(int value, String message) {
        System.out.printf("Moved center of " +"%s" + " = " + "X(" + "%d" + ") " + ", " + "Y(" + "%d" +  ")\n",message, x + value, y + value  );
    }
    //Метод масштабирования периметра и площади фигуры
    @Override
    public void scale(double value, String message) {
        if (value < BASE_ZERO) {
            System.err.println("Value scale cannot be < 0");
        } else {
            System.out.printf("Scaled perimeter of " + "%s" + " = " + "%.1f \n", message, getPerimeter() * value);
            System.out.printf("Scaled area of " + "%s" + " = " + "%.1f\n", message, getArea() * value);
        }
    }
}



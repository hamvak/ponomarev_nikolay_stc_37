package Figures;



/**
 * 07.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Ellipse extends Circle {

    private double radiusTwo;

    public Ellipse(double radius, double radiusTwo, int x, int y) {
        super(radius, x, y);
        if (radiusTwo > BASE_ZERO) {
            this.radiusTwo = radiusTwo;
        } else {
            System.err.println("Radius of RoundFigure cannot be <=0");
        }
    }
    //Метод предка для расчета площади
    @Override
    public double getArea() {
        return PI * radiusTwo * radius;
    }
    //Метод предка для расчета периметра
    //Для удобства формула разбита на несколько частей
    @Override
    public double getPerimeter() {
        return BASE_FOUR * (numerator() / denominator());
    }
    //Числитель формулы периметра
    private double numerator() {
        return (PI * radiusTwo * radius) + rate();
    }
    //Квадрат одного из выражений в числителе формулы периметра
    private double rate() {
        double i = radius - radiusTwo;
        return i * i;
    }
    //Знаменатель формулы периметра
    private double denominator() {
        return radiusTwo + radius;
    }
}




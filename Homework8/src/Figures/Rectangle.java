package Figures;

/**
 * 07.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
public class Rectangle extends Square  {

    private int width;

    public Rectangle(int length, int width, int x, int y) {
        super(length, x, y);
        if (width > BASE_ZERO) {
            this.width = width;
        } else {
            System.err.println("Width of AbstractFigures.RectangularFigures cannot be <=0");
        }

    }
    //Метод предка для расчета площади
    @Override
    public double getArea() {
        return length * width;
    }
    //Метод предка для расчета периметра
    @Override
    public double getPerimeter() {
        return BASE_TWO * (length + width);
    }
}




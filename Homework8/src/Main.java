import Figures.Circle;
import Figures.Ellipse;
import Figures.Rectangle;
import Figures.Square;

public class Main {

    public static void main(String[] args) {
        //Шапка таблицы
        System.out.printf("%10s" + "|" + "%10s" + "|" + "%10s" + "|" +"%5s" + "|" + "%5s" + "|\n"
                          ,"Figure", "Area", "Perimeter", "X", "Y");
        System.out.println("---------------------------------------------");
        //Инициализация объектов
        Circle circle = new Circle(20, 10, 10);
        Rectangle rectangle = new Rectangle(10, 20, 20, 20);
        Square square = new Square(20, 30 ,30);
        Ellipse ellipse = new Ellipse(10, 20, 40 , 40);

        //Расчет площади
        ellipse.getArea();
        //Расчет периметра
        ellipse.getPerimeter();
        //Вывод значений в формате таблицы
        ellipse.print("ellipse");
        System.out.println();
        //Результат масштабирования
        ellipse.scale(0.1, "ellipse");
        //Результат сдвига центра
        ellipse.moveCenterOfFigure(-5, "ellipse");
            // Создаем массив фигур, которые ссылаются на объекты каждого из классов
            // И офорляем цикл выполнения расчета периметра, площади и вывода информации
//        AbstractFigures.GeometricFigures f1 = circle;
//        AbstractFigures.GeometricFigures f2 = rectangle;
//        AbstractFigures.GeometricFigures f3 = square;
//        AbstractFigures.GeometricFigures f4 = ellipse;
//
//        AbstractFigures.GeometricFigures figures[] = {f1, f2, f3, f4};
//        for (int i = 0; i < figures.length; i++) {
//            figures[i].getArea();
//            figures[i].getPerimeter();
//        }
//        square.print("Square");
//        circle.print("Circle");
//        rectangle.print("Rectangle");
//        ellipse.print("Ellipse");


    }
}




package Interface;

/**
 * 07.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
//Интерфейс "масштабирования"
    @FunctionalInterface
public interface ScaleFigure {
    void scale(double value, String message);
}




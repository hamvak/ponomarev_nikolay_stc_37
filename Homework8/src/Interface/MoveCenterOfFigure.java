package Interface;

/**
 * 08.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
//Интерфейс перемещения координат центра
    @FunctionalInterface
public interface MoveCenterOfFigure {

    void moveCenterOfFigure(int value, String message);

}



package AbstractFigures;

/**
 * 07.03.2021
 * Homework8
 *
 * @author Nikolay Ponomarev
 * @version 1.0
 */
//Абстрактный класс для всех геометрических фигур
public abstract class GeometricFigures  {
    //Поля координат центра фигуры
    protected int x;
    protected int y;
    public static final int BASE_ZERO = 0;
    public static final int BASE_TWO = 2;
    public static final int BASE_FOUR = 4;

    protected double area;
    protected double perimeter;

    public GeometricFigures(int x, int y) {
        this.x = x;
        this.y = y;
    }

    //Абстрактный метод площади
    public abstract double getArea();
    //Абстрактный метод периметра
    public abstract double getPerimeter();
    //Метод вывода результата в виде таблицы
    public void print(String message){
        System.out.printf("%10s" + "|" + "%10.1f" + "|" + "%10.1f" + "|" + "%5d" + "|" + "%5d" + "|\n"
                , message, getArea(), getPerimeter(), x, y);
    }
}



